import './App.css';
const data= [
    {
        "id": "#001",
        "name": "Bulbasaur", 
        "type": "Grass",
        "image": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/001.png"
    }, 
    {
        "id": "#004",
        "name": "Charmander", 
        "type": "Fire",
        "image": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/004.png"
    }, 
    {
        "id": "#007",
        "name": "Squirtle",
        "type": "Water",
        "image": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/007.png"
    }
] 

function Pokemon() {
    return <div className="container">
    <h1>Pokemons</h1>
    <h2>This is Pokemon Table</h2>
    <table>
        <thead>
        <tr>
            <th>ID</th>
            <th>Pokemon Name</th>
            <th>Pokemon Image</th>
            <th>Pokemon Type</th>
           
        </tr>
        </thead>
        <tbody>
                    {
                        data.map((item) => (
                            <tr key={item.id}>
                                <td>{item.id}</td>
                                <td>{item.name}</td>
                                <td> <img src= {item.image} alt='pokemon images' height="200" width="200"/></td>
                                <td>{item.type}</td>
                              
                            </tr>
                        ))
                    }
                </tbody>
    </table>
</div>
  
  }
  
export default Pokemon;
